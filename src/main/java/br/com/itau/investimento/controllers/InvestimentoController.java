package br.com.itau.investimento.controllers;

import br.com.itau.investimento.models.DTOs.InvestimentoDTOEntrada;
import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    public ResponseEntity<Investimento> postInvestimento(@RequestBody InvestimentoDTOEntrada investimentoDTOEntrada){
        Investimento investimento = investimentoDTOEntrada.converterParaInvestimento();
        Investimento investimentoObjeto = investimentoService.inserirInvestimento(investimento);

        return ResponseEntity.status(201).body(investimentoObjeto);
    }

    @GetMapping
    public Iterable<Investimento> getInvestimentos(){
        Iterable<Investimento> investimentos = investimentoService.consultarInvestimentos();
        return investimentos;
    }
}
