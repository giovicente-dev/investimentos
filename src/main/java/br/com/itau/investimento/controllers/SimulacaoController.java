package br.com.itau.investimento.controllers;

import br.com.itau.investimento.models.DTOs.SimulacaoDTOEntrada;
import br.com.itau.investimento.models.DTOs.SimulacaoDTOSaidaGet;
import br.com.itau.investimento.models.DTOs.SimulacaoDTOSaidaPost;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping("/investimentos/{id_investimento}/simulacao")
    ResponseEntity<SimulacaoDTOSaidaPost> postSimulacao(@PathVariable(name = "id_investimento") long idInvestimento, @RequestBody @Valid SimulacaoDTOEntrada simulacaoDTOEntrada){

        Simulacao simulacao = new Simulacao(simulacaoDTOEntrada.getNomeInteressado(), simulacaoDTOEntrada.getEmail(), simulacaoDTOEntrada.getValorAplicado(), simulacaoDTOEntrada.getQuantidadeMeses());
        Simulacao simulacaoObjeto = simulacaoService.inserirSimulacao(simulacao, idInvestimento);

        SimulacaoDTOSaidaPost simulacaoDTOSaidaPost = new SimulacaoDTOSaidaPost(simulacaoObjeto.getRendimentoAoMes(), simulacaoObjeto.getMontante());
        return ResponseEntity.status(201).body(simulacaoDTOSaidaPost);
    }

    @GetMapping("/simulacoes")
    public ArrayList<SimulacaoDTOSaidaGet> getSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoService.consultarSimulacoes();
        ArrayList<Simulacao> simulacoesSaida = new ArrayList();

        // preenche o ArrayList com os valores do Iterable
        simulacoes.forEach(simulacoesSaida::add);

        // cria o ArrayList com os campos do DTO de Saída da Simulação para método GET
        ArrayList<SimulacaoDTOSaidaGet> simulacoesSaidaGet = new ArrayList();

        for(int i = 0; i < simulacoesSaida.size(); i++){
            /* para cada ocorrência do ArrayList que recebeu os valores do Iterable,
               instancia um objeto do tipo de Saída da Simulação para método GET */
            SimulacaoDTOSaidaGet simulacaoDTOSaidaGet = new SimulacaoDTOSaidaGet();

            simulacaoDTOSaidaGet.setNomeInteressado(simulacoesSaida.get(i).getNomeInteressado());
            simulacaoDTOSaidaGet.setEmail(simulacoesSaida.get(i).getEmail());
            simulacaoDTOSaidaGet.setValorAplicado(simulacoesSaida.get(i).getValorAplicado());
            simulacaoDTOSaidaGet.setQuantidadeMeses(simulacoesSaida.get(i).getQuantidadeMeses());
            simulacaoDTOSaidaGet.setIdInvestimento(simulacoesSaida.get(i).getInvestimento().getIdInvestimento());

            /* adiciona o objeto recém criado do tipo de Saída da Simulação para método GET
               no ArrayList de retorno do método */
            simulacoesSaidaGet.add(simulacaoDTOSaidaGet);
        }

        return simulacoesSaidaGet;
    }
}
