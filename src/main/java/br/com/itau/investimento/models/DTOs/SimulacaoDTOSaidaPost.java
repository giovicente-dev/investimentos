package br.com.itau.investimento.models.DTOs;

import org.springframework.format.annotation.NumberFormat;

public class SimulacaoDTOSaidaPost {

    private double rendimentoAoMes;

    @NumberFormat(pattern = "#,###,###,###,###.##")
    private double montante;

    public SimulacaoDTOSaidaPost() { }

    public SimulacaoDTOSaidaPost(double rendimentoAoMes, double montante) {
        this.rendimentoAoMes = rendimentoAoMes;
        this.montante = montante;
    }

    public double getRendimentoAoMes() { return rendimentoAoMes; }

    public void setRendimentoAoMes(double rendimentoAoMes) { this.rendimentoAoMes = rendimentoAoMes; }

    public double getMontante() { return montante; }

    public void setMontante(double montante) { this.montante = montante; }

}
