package br.com.itau.investimento.models.DTOs;

import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.Email;

public class SimulacaoDTOEntrada {

    private String nomeInteressado;

    @Email
    private String email;

    @NumberFormat(pattern = "#,###,###,###,###.##")
    private double valorAplicado;

    private int quantidadeMeses;

    public SimulacaoDTOEntrada() { }

    public SimulacaoDTOEntrada(String nomeInteressado, String email, double valorAplicado, int quantidadeMeses) {
        this.nomeInteressado = nomeInteressado;
        this.email = email;
        this.valorAplicado = valorAplicado;
        this.quantidadeMeses = quantidadeMeses;
    }

    public String getNomeInteressado() { return nomeInteressado; }

    public void setNomeInteressado(String nomeInteressado) { this.nomeInteressado = nomeInteressado; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public double getValorAplicado() { return valorAplicado; }

    public void setValorAplicado(double valorAplicado) { this.valorAplicado = valorAplicado; }

    public int getQuantidadeMeses() { return quantidadeMeses; }

    public void setQuantidadeMeses(int quantidadeMeses) { this.quantidadeMeses = quantidadeMeses; }


}
