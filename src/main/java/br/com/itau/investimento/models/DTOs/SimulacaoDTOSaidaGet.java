package br.com.itau.investimento.models.DTOs;

public class SimulacaoDTOSaidaGet {

    private String nomeInteressado;
    private String email;
    private double valorAplicado;
    private int quantidadeMeses;
    private long idInvestimento;

    public SimulacaoDTOSaidaGet() { }

    public String getNomeInteressado() { return nomeInteressado; }

    public void setNomeInteressado(String nomeInteressado) { this.nomeInteressado = nomeInteressado; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public double getValorAplicado() { return valorAplicado; }

    public void setValorAplicado(double valorAplicado) { this.valorAplicado = valorAplicado; }

    public int getQuantidadeMeses() { return quantidadeMeses; }

    public void setQuantidadeMeses(int quantidadeMeses) { this.quantidadeMeses = quantidadeMeses; }

    public long getIdInvestimento() { return idInvestimento; }

    public void setIdInvestimento(long idInvestimento) { this.idInvestimento = idInvestimento; }

}
