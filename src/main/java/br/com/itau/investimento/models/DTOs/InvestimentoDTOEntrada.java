package br.com.itau.investimento.models.DTOs;

import br.com.itau.investimento.models.Investimento;

public class InvestimentoDTOEntrada {

    private String nome;
    private double rendimentoAoMes;

    public InvestimentoDTOEntrada(){ }

    public String getNome() { return nome; }

    public void setNome(String nome) { this.nome = nome; }

    public double getRendimentoAoMes() { return rendimentoAoMes; }

    public void setRendimentoAoMes(double rendimentoAoMes) { this.rendimentoAoMes = rendimentoAoMes; }

    public Investimento converterParaInvestimento(){
        Investimento investimento = new Investimento();

        investimento.setNome(this.nome);
        investimento.setRendimentoAoMes(this.rendimentoAoMes);

        return investimento;
    }
}
