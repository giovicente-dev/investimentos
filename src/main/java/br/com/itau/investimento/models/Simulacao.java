package br.com.itau.investimento.models;

import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idSimulacao;

    private String nomeInteressado;

    private String email;

    private double valorAplicado;

    private int quantidadeMeses;

    private double rendimentoAoMes;

    private double montante;

    @ManyToOne
    private Investimento investimento;

    public Simulacao(){ }

    public Simulacao(String nomeInteressado, String email, double valorAplicado, int quantidadeMeses) {
        this.nomeInteressado = nomeInteressado;
        this.email = email;
        this.valorAplicado = valorAplicado;
        this.quantidadeMeses = quantidadeMeses;
    }

    public long getIdSimulacao() { return idSimulacao; }

    public void setIdSimulacao(long idSimulacao) { this.idSimulacao = idSimulacao; }

    public String getNomeInteressado() { return nomeInteressado; }

    public void setNomeInteressado(String nomeInteressado) { this.nomeInteressado = nomeInteressado; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public double getValorAplicado() { return valorAplicado; }

    public void setValorAplicado(double valorAplicado) { this.valorAplicado = valorAplicado; }

    public int getQuantidadeMeses() { return quantidadeMeses; }

    public void setQuantidadeMeses(int quantidadeMeses) { this.quantidadeMeses = quantidadeMeses; }

    public double getRendimentoAoMes() { return rendimentoAoMes; }

    public void setRendimentoAoMes(double rendimentoAoMes) { this.rendimentoAoMes = rendimentoAoMes; }

    public double getMontante() { return montante; }

    public void setMontante(double montante) {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
    }

    public Investimento getInvestimento() { return investimento; }

    public void setInvestimento(Investimento investimento) { this.investimento = investimento; }
}
