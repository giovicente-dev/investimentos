package br.com.itau.investimento.models;

import javax.persistence.*;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idInvestimento;

    @Column(unique = true)
    private String nome;

    private double rendimentoAoMes;

    public Investimento(){ }

    public Investimento(long idInvestimento) { this.idInvestimento = idInvestimento; }

    public Investimento(String nome, double rendimentoAoMes) {
        this.nome = nome;
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public long getIdInvestimento() { return idInvestimento; }

    public String getNome() { return nome; }

    public void setNome(String nome) { this.nome = nome; }

    public double getRendimentoAoMes() { return rendimentoAoMes; }

    public void setRendimentoAoMes(double rendimentoAoMes) { this.rendimentoAoMes = rendimentoAoMes; }

}
