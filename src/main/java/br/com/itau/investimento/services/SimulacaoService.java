package br.com.itau.investimento.services;

import br.com.itau.investimento.models.Investimento;
import br.com.itau.investimento.models.Simulacao;
import br.com.itau.investimento.repositories.InvestimentoRepository;
import br.com.itau.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Simulacao inserirSimulacao(Simulacao simulacao, long idInvestimento){
        simulacao.setRendimentoAoMes(obterRendimento(idInvestimento));
        simulacao.setMontante(calcularMontante(simulacao.getQuantidadeMeses(), simulacao.getRendimentoAoMes(), simulacao.getValorAplicado()));

        Investimento investimento = new Investimento(idInvestimento);
        simulacao.setInvestimento(investimento);

        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);
        return simulacaoObjeto;
    }

    public double obterRendimento(long id){
        Investimento investimento = new Investimento();
        investimento = investimentoRepository.findByIdInvestimento(id);
        double rendimento = investimento.getRendimentoAoMes();

        return rendimento;
    }

    public double calcularMontante(int quantidadeMeses, double rendimentoAoMes, double valorAplicado){
        double montante = (valorAplicado) + ((valorAplicado * rendimentoAoMes) * quantidadeMeses);
        return montante;
    }

    public Iterable<Simulacao> consultarSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        return simulacoes;
    }

}
